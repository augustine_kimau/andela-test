package com.example.kimau.myapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    static final String TAG = "MainActivity";

    ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iv = findViewById(R.id.imageView);
    }

    public void handleFetchImgBt(View view) {

        new AsyncTask<Void, Void, byte[]>() {
            @Override
            protected byte[] doInBackground(Void... voids) {
                return fetchImage("https://dummyimage.com/600x400/000/fff", 5);
            }

            @Override
            protected void onPostExecute(byte[] bytes) {
                iv.setImageBitmap(BitmapFactory.decodeByteArray(bytes, 0, bytes.length));
            }
        }.execute();
    }

    public static byte[] fetchImage(String url, int recursiveCount) {

        Log.d(TAG, "fetching image: " + url);

        HttpURLConnection connection = null;
        InputStream inputStream = null;

        byte[] imageBytes = null;

        try {
            connection = (HttpURLConnection) new URL(url).openConnection();
            inputStream = connection.getInputStream();
            imageBytes = getBytesFromStream(inputStream);
        } catch (IOException ex) {
            Log.d(TAG, ex.getMessage());
        } finally {
            closeInputStream(inputStream);
            disconnectConnection(connection);
        }

        if (imageBytes == null || imageBytes.length == 0) {
            imageBytes = null;
            if (recursiveCount < 3) return fetchImage(url, ++recursiveCount);
        }

        return imageBytes;
    }

    public static byte[] getBytesFromStream(InputStream inputStream) {
        try {
            byte[] data = new byte[0];
            byte[] buffer = new byte[5 * 1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) > 0) {
                // construct large enough array for all the data we now have
                byte[] newData = new byte[data.length + bytesRead];
                // copy data previously read
                System.arraycopy(data, 0, newData, 0, data.length);
                // append data newly read
                System.arraycopy(buffer, 0, newData, data.length, bytesRead);
                // discard the old array in favour of the new one
                data = newData;
            }
            return data;
        } catch (IOException ex) {
            Log.d(TAG, ex.getMessage());
        }

        return null;
    }

    public static int ordinalIndexOf(String str, String substr, int n) {
        int pos = str.indexOf(substr);
        while (--n > 0 && pos != -1)
            pos = str.indexOf(substr, pos + 1);
        return pos;
    }

    private static void closeReader(Reader reader) {
        try {
            if (reader != null) {
                reader.close();
            }
        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    private static void closeInputStream(InputStream inputStream) {
        try {
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (IOException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    private static void closeOutputStream(OutputStream outputStream) {
        try {
            if (outputStream != null) {
                outputStream.close();
            }
        } catch (IOException e) {
        }
    }

    private static void disconnectConnection(HttpURLConnection connection) {
        if (connection != null) {
            connection.disconnect();
        }
    }
}
